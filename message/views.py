from message.models import Message
from message.serializers import MessageSerializer

from rest_framework import viewsets
from drf_spectacular.utils import extend_schema_view, extend_schema


@extend_schema_view(
    list=extend_schema(
        description='Получить информацию о сообщениях'
    ),
    retrieve=extend_schema(
        description='Получить информацию о сообщении по id'
    ),
    create=extend_schema(
        description='Создать сообщение'
    ),
    update=extend_schema(
        description='Обновить данные о сообщении'
    ),
    partial_update=extend_schema(
        description='Обновить данные о сообщении'
    ),
    destroy=extend_schema(
        description='Удалить сообщение'
    )
)
class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

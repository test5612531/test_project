from celery import shared_task
import requests

from django.db.models import Q
from django.utils import timezone
from django.conf import settings

from mailing.models import Mailing
from client.models import Client
from message.models import Message


@shared_task(name='mailing_routine')
def mailing_routine():
    active_mailings = Mailing.objects.filter(start_time__lte=timezone.now(), end_time__gt=timezone.now())

    for mailing in active_mailings:
        clients = Client.objects.filter(
            Q(tag=mailing.tag_filter) & Q(mobile_code=mailing.mobile_code_filter)
        )
        messages_created = []
        for client in clients:
            if Message.objects.filter(Q(client=client) & Q(mailing=mailing)).exists():
                continue

            messages_created.append(Message(
                created_at=timezone.now(),
                status='CREATED',
                mailing=mailing,
                client=client
            ))
        Message.objects.bulk_create(messages_created)


@shared_task(name='send_message')
def send_messages():
    messages_to_send = Message.objects.filter(Q(status='CREATED') | Q(status='FAILED'))

    for message in messages_to_send:
        token = settings.MAILING_TOKEN
        url = 'https://probe.fbrq.cloud/v1/send/'
        headers = {'Authorization': f'Bearer {token}'}
        data = {
            'id': message.id,
            'phone': int(message.client.phone_number),
            'text': message.mailing.text
        }
        response = requests.post(f'{url}{message.id}', json=data, headers=headers)
        if response.status_code == 200:
            message.status = 'SENT'
            message.save()
        else:
            message.status = 'FAILED'
            message.save()
            print(f'message failed, code: {response.status_code}')

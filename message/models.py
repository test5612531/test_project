from django.db import models
from mailing.models import Mailing
from client.models import Client


class Message(models.Model):
    Statuses = models.TextChoices('Statuses', 'CREATED SENT FAILED')
    created_at = models.DateTimeField()
    status = models.CharField(choices=Statuses.choices, max_length=7, default='CREATED')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

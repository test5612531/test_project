# Generated by Django 4.2.3 on 2023-07-30 03:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('message', '0002_alter_message_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.CharField(choices=[('CREATED', 'Created'), ('SENT', 'Sent'), ('FAILED', 'Failed')], default='CREATED', max_length=7),
        ),
    ]

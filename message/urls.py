from rest_framework.routers import DefaultRouter

from message.views import MessageViewSet

messages_router = DefaultRouter()
messages_router.register(r'messages', MessageViewSet)

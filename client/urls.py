from rest_framework.routers import DefaultRouter

from client.views import ClientViewSet

clients_router = DefaultRouter()
clients_router.register('clients', ClientViewSet)

from django.db import models
from timezone_field import TimeZoneField


class Client(models.Model):
    phone_number = models.CharField(max_length=11)
    mobile_code = models.CharField(max_length=3)
    tag = models.TextField()
    timezone = TimeZoneField()

from rest_framework import serializers
from client.models import Client
from zoneinfo import available_timezones


class ClientSerializer(serializers.ModelSerializer):
    timezone = serializers.ChoiceField(choices=available_timezones())

    class Meta:
        model = Client
        fields = '__all__'

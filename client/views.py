from client.models import Client
from client.serializers import ClientSerializer

from rest_framework import viewsets
from drf_spectacular.utils import extend_schema_view, extend_schema


@extend_schema_view(
    list=extend_schema(
        description='Получить информацию о всех клиентах'
    ),
    retrieve=extend_schema(
        description='Получить информацию о клиенте по id'
    ),
    create=extend_schema(
        description='Создать клиента'
    ),
    update=extend_schema(
        description='Обновить данные о клиенте'
    ),
    partial_update=extend_schema(
        description='Обновить данные о клиенте'
    ),
    destroy=extend_schema(
        description='Удалить клиента'
    )
)
class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


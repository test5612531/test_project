from django.db import models
from django.contrib.auth.models import User


class Mailing(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    text = models.TextField()
    tag_filter = models.TextField(default='')
    mobile_code_filter = models.CharField(max_length=3, default='123')

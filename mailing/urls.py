from rest_framework.routers import DefaultRouter

from mailing.views import MailingViewSet

mailing_router = DefaultRouter()
mailing_router.register(r'mailing', MailingViewSet)


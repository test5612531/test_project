from mailing.models import Mailing
from mailing.serializers import MailingSerializer
from message.models import Message
from message.serializers import MessageSerializer

from rest_framework import viewsets
from rest_framework.response import Response
from drf_spectacular.utils import extend_schema_view, extend_schema


@extend_schema_view(
    list=extend_schema(
        description='Получить статистику о рассылках'
    ),
    retrieve=extend_schema(
        description='Получить статистику о рассылке по id'
    ),
    create=extend_schema(
        description='Создать рассылку'
    ),
    update=extend_schema(
        description='Обновить данные о рассылке'
    ),
    partial_update=extend_schema(
        description='Обновить данные о рассылке'
    ),
    destroy=extend_schema(
        description='Удалить рассылку'
    )
)
class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def list(self, request, *args, **kwargs):
        response_body = []

        for mailing in Mailing.objects.all():
            messages = Message.objects.filter(mailing=mailing)
            response_body.append({
                'id': mailing.id,
                'tag_filter': mailing.tag_filter,
                'mobile_code_filter': mailing.mobile_code_filter,
                'messages_total': messages.count(),
                'messages_created': messages.filter(status='CREATED').count(),
                'messages_sent': messages.filter(status='SENT').count(),
                'messages_failed': messages.filter(status='FAILED').count()
            })

        return Response(response_body)

    def retrieve(self, request, *args, **kwargs):
        mailing = self.get_object()
        messages = Message.objects.filter(mailing=mailing)
        response = self.get_serializer(mailing).data
        response['messages'] = MessageSerializer(messages, many=True).data
        return Response(response)

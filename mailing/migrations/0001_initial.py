# Generated by Django 4.2.3 on 2023-07-28 14:59

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Mailing',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.DateTimeField(verbose_name='Time and date of the start of mailing')),
                ('end_time', models.DateTimeField(verbose_name='Time and date of mailing deadline')),
                ('text', models.TextField(verbose_name='Mailing text')),
            ],
        ),
    ]

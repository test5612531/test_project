import os
from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'djangoProject.settings')

app = Celery('djangoProject')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.beat_schedule = {
    'mailing': {
        'task': 'mailing_routine',
        'schedule': 30.0,
    },
    'send_messages': {
        'task': 'send_message',
        'schedule': 30.0,
    }
}
